import os, sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QLabel, QVBoxLayout, QPushButton
from PyQt5.QtCore import QCoreApplication


class SlideshowWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__()
        self.parent = parent

        self.initUi()

    def initUi(self):
        label = QLabel()
        label.setText("Hello, world!")

        close_button = QPushButton()
        close_button.setText("Exit")
        close_button.clicked.connect(self.close_button_clicked)

        layout = QVBoxLayout()
        layout.addWidget(label)
        layout.addWidget(close_button)
        self.setLayout(layout)

    def close_button_clicked(self):
        self.close()


class DashboardWidget(QWidget):
    def __init__(self):
        super().__init__()

        # window attribs
        self.slideshow = None
        self.settings = None

        # build the ui.
        self.initUi()

    def initUi(self):
        label = QLabel()
        label.setText("Hello, world!")

        close_button = QPushButton()
        close_button.setText("Exit")
        close_button.clicked.connect(self.close_button_clicked)

        slideshow_button = QPushButton()
        slideshow_button.setText("Slideshow")
        slideshow_button.clicked.connect(self.slideshow_button_clicked)

        layout = QVBoxLayout()
        layout.addWidget(label)
        layout.addWidget(slideshow_button)
        layout.addWidget(close_button)
        self.setLayout(layout)

    def close_button_clicked(self):
        #QCoreApplication.instance().quit()
        os.system("sudo reboot")

    def slideshow_button_clicked(self):
        if self.slideshow is None:
            self.slideshow = SlideshowWidget(self)
            self.slideshow.setMinimumSize(self.width(), self.height())

        self.slideshow.showFullScreen()

def main():
    app = QApplication(sys.argv)

    # get current screen size.
    sz = app.primaryScreen().size()

    # set up the main window
    window = QMainWindow()
    window.setWindowTitle("Test Python Kiosk")
    window.setBaseSize(sz)
    window.setMinimumSize(sz.width(), sz.height())

    print("Size: {0} x {1}".format(sz.width(), sz.height()))

    # set dashboard as central widget in main window
    dashboard = DashboardWidget()
    window.setCentralWidget(dashboard)

    window.showFullScreen()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()

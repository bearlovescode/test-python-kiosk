from setuptools import setup, find_packages


setup(
    name='test-python-kiosk',
    version="0.0.1",
    packages=find_packages()
)